#!/usr/bin/python3

def insert_information(orientation):
    flux_type = 'Aqueous Volumetric Flux'
    start = {
        'i' : 17,
        'j' : 77,
        'k' : 11
    }
    end = {
        'i' : 25,
        'j' : 71,
        'k' : 11
    }
    inner_object = {
        'flux_type' : flux_type,
        'orientation' : orientation,
        'start' : start,
        'end' : end
    }
    return inner_object


def main():
    parsed_surface_file = []

    parsed_surface_file.append(insert_information('East Surface'))
    parsed_surface_file.append(insert_information('West Surface'))

    print(parsed_surface_file)
    print('\n')

    for element in parsed_surface_file:
        print(element['orientation'])

if __name__ == '__main__':
    main()
